<?php

return [
    'api' => [
        'endpoint' => env('MJML_API_ENDPOINT', 'https://mjml.weanswer.it/api/v1/mjml'),
        'key' => env('MJML_API_KEY'),
        'secret' => env('MJML_API_SECRET')
    ],
];

