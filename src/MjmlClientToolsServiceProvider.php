<?php

namespace MjmlClientTools;

use Illuminate\Support\ServiceProvider;

class MjmlClientToolsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->publishes([
            __DIR__ . '/../config/mjml.php' => config_path('mjml.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/Mjml.php.stub' => app_path('Mjml.php'),
        ], 'model');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/mjml.php', 'mjml');
    }

}
