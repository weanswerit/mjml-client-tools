<?php

namespace MjmlClientTools;

class Mjml
{
    public static function getHtml(int $templateId, $variables = [])
    {
        $requestData = [
            'template_id' => $templateId,
            'variables' => json_encode($variables)
        ];


        $session = curl_init(config('mjml.api.endpoint') . '/html');
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $requestData);

        curl_setopt($session, CURLOPT_HTTPHEADER, array(
            'x-api-key: ' . config('mjml.api.key'),
            'x-api-secret: ' . config('mjml.api.secret')
        ));

        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($session);
        curl_close($session);

        $decodedResponse = json_decode($response, true);

        if (!$decodedResponse) {
            return $response;
        }

        return $decodedResponse;
    }

    public static function getBladeHtml(int $templateId)
    {
        $requestData = [
            'template_id' => $templateId,
        ];

        $session = curl_init(config('mjml.api.endpoint') . '/blade');
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $requestData);

        curl_setopt($session, CURLOPT_HTTPHEADER, array(
            'x-api-key: ' . config('mjml.api.key'),
            'x-api-secret: ' . config('mjml.api.secret')
        ));

        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($session);
        curl_close($session);

        $decodedResponse = json_decode($response, true);

        if (!$decodedResponse) {
            return $response;
        }

        return $decodedResponse;
    }
}
